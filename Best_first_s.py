from queue import PriorityQueue

def Bestfirst(graph,H_D,src,des):
    vis=[]
    s=0
    pq=PriorityQueue()
    pq.put((H_D[src],src,0))
    fq=[]
    vis.append(src)
    while pq.empty()==False:
        ans=pq.get()
        u=ans[1]
        s=s+ans[2]
        fq.append(u)
        if(u==des):
            break
        for c,v in graph[u]:
            if c not in vis:
                vis.append(c)
                pq.put((H_D[c],c,v))
    print(fq)
    print("Length from source to destination :  ",s)


graph={
    'A':[('B',11),('D',7),('C',14)],
    'B':[('A',11),('E',15)],
    'C':[('A',14),('E',8),('F',10)],
    'D':[('A',7),('F',25)],
    'E':[('B',15),('C',8),('H',9)],
    'F':[('C',10),('D',25),('G',20)],
    'G':[('F',20),('H',10)],
    'H':[('E',9),('G',10)]
}
H_D={
    'A':40,
    'B':32,
    'C':25,
    'D':35,
    'E':19,
    'F':17,
    'G':0,
    'H':10
}
src=input("Enter the source node : ")
des=input("Enter the destination node : ")
Bestfirst(graph,H_D,src,des)