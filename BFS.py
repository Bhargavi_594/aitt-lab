def bfs(node, graph1, visited, component):
    component.append(node) 
   # print
    while (len(component))!=0:
        v=component.pop(0)
        print(v,end=" ")
        visited[v]=True
        i=0
        for child in graph1[v]:
            if child==1 and not visited[i]:
                component.append(i)
            i=i+1

graph1 = [[0,0,1,0,0],[0,0,1,1,0],[1,1,0,0,1],[0,1,0,0,1],[0,0,1,1,0]]
node = 0
visited = [False]*len(graph1) 
component = []
bfs(node, graph1, visited, component)
print(f"Following is the Depth-first search: {component}") 