graph = {
    'A': ['B', 'C', 'D'],
    'B': ['A'],
    'C': ['A', 'D'],
    'D': ['A', 'C', 'E'],
    'E': ['D','F'],
    'F': ['G','H','E'],
    'G': ['I','H','F'],
    'H': ['G','F'],
    'I': ['G'],
}
res1=[]
res2=[]

def BDS(node,goal):
    visited1 = []
    visited2 = []

    queue1 = []
    queue2 = []

    visited1.append(node)
    queue1.append(node)

    visited2.append(goal)
    queue2.append(goal)

    while queue1 and queue2:
        v1 = queue1.pop()
        v2=queue2.pop()

        if v1 not in res2:
            res1.append(v1)

        if v2 not in res1:
            res2.append(v2)

        if(v1==v2) or v2 in res1 or v1 in res2:
            break
                
        for neigh in graph[v1]:
            if neigh not in visited1:
                visited1.append(neigh)
                queue1.append(neigh)

        for i in graph[v2]:
            if i not in visited2:
                visited2.append(i)
                queue2.append(i)
    
    for ele in reversed(res2):
        res1.append(ele)
    print("Path from source to destination :",res1)

BDS('I','D')
