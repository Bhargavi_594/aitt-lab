def dfs(node, graph1, visited, component):
    component.append(node) 
    visited[node] = True
    i=0  
    for child in graph1[node]:
        if (child==1 and not visited[i]):
            dfs(i, graph1, visited, component)
        i=i+1
graph1 = [[0,1,0,0,1,0],
          [0,0,1,1,0,0],
          [0,0,0,0,0,0],
          [0,0,0,0,0,1],
          [0,0,0,0,0,1],
          [0,0,0,0,0,0]]
node = 0
visited = [False]*len(graph1) 
component = []
dfs(node, graph1, visited, component)
print(f"Following is the Depth-first search: {component}") 